'''
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import argparse
import subprocess
from userInput import user_input
parser = argparse.ArgumentParser(description='Script to batch convert multichannel prints to Higher Order Ambisonic with SATIE.')

parser.add_argument('input_dir', type=str, help='Must provide a directory with print (as mono wave files) to convert.')

parser.add_argument('output_dir', type=str, help='The directory to write the new ambi file.')

parser.add_argument('order', type=int, help='Ambisonic order of the encoded file') 

args = parser.parse_args()

if args.input_dir:
    inDir = args.input_dir 

if args.output_dir:
    outDir = args.output_dir

if args.order:
    order = str(args.order)

stereoMono = user_input('Your file is stereo and you need to provide a mono file. Do you want to proceed to a conversion?')
overwrite = user_input('You are about to overwrite a file with the same name. Do you want to overwrite?')

p = subprocess.Popen(['sclang', 'satieNRT_Python.scd', inDir, order], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

print(p.communicate()[0].decode('ascii'))  # Will output SC log at the end of the conversion or when error
